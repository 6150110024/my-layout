package com.example.app;

import androidx.appcompat.app.AppCompatActivity;

import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        Button button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity3.this, MainActivity2.class);
                startActivity(intent);
            }
        });
    }
    public void browser1(View View){
        Intent browserIntent =new Intent(Intent.ACTION_VIEW,Uri.parse("https://web.facebook.com/banthitabam30576"));
        startActivity(browserIntent);

    }
    public void browser2(View View){
        Intent browserIntent =new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.instagram.com/fb.bambam/?hl=th"));
        startActivity(browserIntent);

    }
}